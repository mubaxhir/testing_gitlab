from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from organizations.backends import invitation_backend, registration_backend

from accounts.views import UserDirectoryView
from vendors.views import base, home, LoginView, LogOutView, signup, IdCardDetail, DashboardView, profile_update

urlpatterns = [
                  path("", home, name="home"),
                  path("auth/", include('accounts.urls')),
                  path("admin/", admin.site.urls),
                  path("organizations/", include("orghub_organizations.urls"), ),
                  path("invite/", include(invitation_backend().get_urls())),
                  path("register/", include(registration_backend().get_urls()), name="register"),
                  path("login/", LoginView.as_view(), name='login'),
                  path("logout/", LogOutView.as_view(), name='logout'),
                  path("dashboard/", DashboardView.as_view(), name='dashboard'),
                  path("base/", base, name='base'),
                  path('signup/', signup, name="signup"),
                  path('profile/update/', profile_update, name="update"),
                  path('id_card/<pk>/', IdCardDetail.as_view(), name="id_card"),
                  path('user-directory/', UserDirectoryView.as_view(), name="user_directory"),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
