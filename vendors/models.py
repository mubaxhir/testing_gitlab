from django.contrib.auth.models import Permission
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from organizations.abstract import (
    AbstractOrganization,
    AbstractOrganizationOwner,
    AbstractOrganizationUser,
)


class Vendor(AbstractOrganization):
    street_address = models.CharField(max_length=100, default="")
    city = models.CharField(max_length=100, default="")
    account = models.ForeignKey(
        "accounts.Account", on_delete=models.CASCADE, related_name="vendors"
    )


class VendorUser(AbstractOrganizationUser):
    user_type = models.CharField(max_length=1, default="")
    permissions = models.ManyToManyField(Permission, blank=True)


class VendorOwner(AbstractOrganizationOwner):
    pass


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_{0}/{1}'.format(instance.user.id, filename)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='user_profile')
    phone = models.CharField(max_length=50, blank=True, null=True)
    date_of_birth = models.DateField(blank=True, null=True)
    occupation = models.CharField(max_length=50, blank=True, null=True)
    country = models.CharField(max_length=50, blank=True, null=True)
    company_name = models.CharField(max_length=100, blank=True, null=True)
    email = models.EmailField(max_length=150)
    address = models.TextField(max_length=500, null=True, blank=True)
    company_telephone = models.CharField(max_length=50, null=True, blank=True)
    about = models.TextField(null=True, blank=True, max_length=2000)
    picture = models.ImageField(upload_to=user_directory_path, blank=True)


@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.user_profile.save()
