from datetime import timedelta

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views import generic
from django.views.generic import DetailView, TemplateView
from django.conf import settings
from django.http import Http404,HttpResponse,JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.files import File
import boto3,botocore,base64,os
from qrcode import make
import barcode
from barcode.writer import ImageWriter
from PIL import Image, ImageFile,ImageFont,ImageDraw
from .forms import LoginForm, UserUpdateForm, ProfileUpdateForm
from .forms import SignUpForm
from .models import Profile


def home(request):
    return render(request, 'home.html', {})


class DashboardView(LoginRequiredMixin, TemplateView):
    login_url = '/login/'
    template_name = 'dashboard_base.html'


def base(request):
    return render(request, 'home.html', {})


class IdCardDetail(DetailView):
    template_name = 'vendors/id_card.html'
    model = User

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        user = kwargs['object']
        # user = OrganizationUser.objects.filter(user=user).first()
        issue_date = user.date_joined if user.date_joined else 'N/A'
        expiry_date = user.date_joined + timedelta(days=365) if issue_date else 'N/A'
        context['qr_code_data'] = {
            'id': user.pk,
            'name': user.get_full_name() if user.first_name else 'N/A',
            'dob': user.user_profile.date_of_birth.strftime("%d/%m/%Y") if user.user_profile.date_of_birth else 'N/A',
            'occupation': user.user_profile.occupation if user.user_profile.occupation else 'N/A',
            'location': user.user_profile.country if user.user_profile.country else 'N/A',
            'organization_name': user.organization.name if hasattr(kwargs['object'], 'organization') else 'N/A',
            'username': user.username if user.username else 'N/A',
            'email': user.email if user.email else 'N/A',
            'issue_date': issue_date.strftime("%d/%m/%Y") if issue_date else 'N/A',
            'expiry_date': expiry_date.strftime("%d/%m/%Y") if expiry_date else 'N/A',
        }

        qr=make(context['qr_code_data'])  
        qr.save("static/img/qrcode.png")

        encoder = barcode.get_barcode_class('code128')
        bar = encoder("abcd_efg123456789",writer=ImageWriter())
        bar.save('static/img/barcode')

        try:
            profile = Profile.objects.filter(user=self.request.user)
            profile=profile[0]
        except Exception as e:
            print(e)
        
        

        context['profile'] = profile.picture

        return context


class LoginView(generic.FormView):
    form_class = LoginForm
    success_url = reverse_lazy('dashboard')
    template_name = 'accounts/login.html'

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)

        if user is not None and user.is_active:
            login(self.request, user)
            return super(LoginView, self).form_valid(form)
        else:
            return self.form_invalid(form)


class LogOutView(generic.RedirectView):
    url = reverse_lazy('home')

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogOutView, self).get(request, *args, **kwargs)


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.user_profile.phone = form.cleaned_data.get('phone')
            user.user_profile.company_name = form.cleaned_data.get('company_name')
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('dashboard')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


def profile_update(request):
    profile = Profile.objects.get(user=request.user)
    if request.method == 'POST':
        user_form = UserUpdateForm(request.POST, instance=request.user)
        profile_form = ProfileUpdateForm(request.POST, request.FILES, instance=profile)

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            update_card(request)
            return redirect('dashboard')

    else:
        user_form = UserUpdateForm(instance=request.user)
        profile_form = ProfileUpdateForm(instance=profile)

    context = {
        'u_form': user_form,
        'p_form': profile_form
    }


    
    return render(request, 'vendors/profile_update.html', context)


def handle_uploaded_file(f):
    dest = open('/static/media', 'wb')
    for chunk in f.chunks():
        dest.write(chunk)
    dest.close()

        



def update_card(request):
    
    user = Profile.objects.filter(user=request.user)
    user=user[0]
    photo = user.picture

    qr_code_data = {
        'id': user.pk,
        'name': user.user if user.user else 'N/A',
        'dob': user.date_of_birth.strftime("%d/%m/%Y") if user.date_of_birth else 'N/A',
        'occupation': user.occupation if user.occupation else 'N/A',
        'location': user.country if user.country else 'N/A',
        'username': user.user if user.user else 'N/A',
        'email': user.email if user.email else 'N/A'
    }

    qr=make(qr_code_data)  
    qr.save("static/img/qrcode.png")

    encoder = barcode.get_barcode_class('code128')
    bar = encoder("abcd_efg123456789",writer=ImageWriter())
    bar.save('static/img/barcode')
    
    photo_path=str(user.user)+"_photo.jpg"
    card_path=str(user.user)+"_card.jpg"
    photo = user.picture
    ImageFile.MAXBLOCK = 2**20
    user.picture.open()
    photo = Image.open(photo)
    photo.save(photo_path, "JPEG", quality=80, optimize=True, progressive=True)
    qrcode = Image.open('static/img/qrcode.png')
    barCode = Image.open('static/img/barcode.png')
    background = Image.open('static/img/card_template.jpg')
    newsize = (150, 180) 
    photo = photo.resize(newsize) 
    offset = (18, 85)
    background.paste(photo, offset)
    newsize = (120, 120) 
    qrcode = qrcode.resize(newsize) 
    offset = (390, 122)
    background.paste(qrcode, offset)
    newsize = (120, 30) 
    barCode = barCode.resize(newsize) 
    offset = (390, 240)
    background.paste(barCode, offset)
    draw = ImageDraw.Draw(background)
    # font = ImageFont.truetype("sans-serif.ttf", 16)
    font = ImageFont.truetype("arial.ttf", 28)
    draw.text((194, 90),str(user.user),(0,0,0),font=font)
    draw.text((400, 85),str(user.country),(0,0,0),font=font)
    draw.text((194, 143),str(user.date_of_birth),(0,0,0),font=font)
    draw.text((194, 190),str(user.occupation),(0,0,0),font=font)
    font = ImageFont.truetype("arial.ttf", 15)
    draw.text((194, 240),"N/A",(0,0,0),font=font)
    draw.text((263, 240),"N/A",(0,0,0),font=font)
    background.save(card_path)

    basedir = settings.BASE_DIR


    # s3 object
    s3 = boto3.resource('s3', aws_access_key_id=str(settings.AWS_ACCESS_KEY_ID), aws_secret_access_key=str(settings.AWS_SECRET_ACCESS_KEY))
    if s3.Bucket(str(settings.AWS_STORAGE_BUCKET_NAME)) not in s3.buckets.all():
        print("creating bucket")
        s3.create_bucket(Bucket=str(settings.AWS_STORAGE_BUCKET_NAME), CreateBucketConfiguration={'LocationConstraint': "us-west-2"})
    

    # upload profile photo and remove from local
    try:
        s3.Object(str(settings.AWS_STORAGE_BUCKET_NAME), photo_path).load()
        print("file already exists")
        os.remove(basedir+'\\'+photo_path)
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            with open(photo_path, 'rb') as data:
                s3.meta.client.upload_fileobj(data,str(settings.AWS_STORAGE_BUCKET_NAME), photo_path)
            os.remove(basedir+'\\'+photo_path)

    # upload IDcard and remove from local
    try:
        s3.Object(str(settings.AWS_STORAGE_BUCKET_NAME), card_path).load()
        print("file already exists")
        os.remove(basedir+'\\'+card_path)
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            with open(card_path, 'rb') as data:
                s3.meta.client.upload_fileobj(data,str(settings.AWS_STORAGE_BUCKET_NAME), card_path)
            os.remove(basedir+'\\'+card_path)