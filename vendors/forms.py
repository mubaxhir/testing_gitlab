from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, ButtonHolder, Submit
from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from .models import Profile


class LoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            'username',
            'password',
            ButtonHolder(
                Submit('login', 'Login', css_class='btn btn-primary btn-block')
            )
        )


class SignUpForm(UserCreationForm):
    email = forms.EmailField()
    phone = forms.CharField(max_length=50, required=True)
    company_name = forms.CharField(max_length=100, required=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', 'phone', 'company_name')


# class Update_user_profile(UserChangeForm):
#     email = forms.EmailField()
#     phone = forms.CharField(max_length=50, required=True)
#     company_name = forms.CharField(max_length=100, required=True)
#
#     class Meta:
#         model = User
#         fields = ('username', 'email', 'password1', 'password2', 'phone', 'company_name')


class UserUpdateForm(forms.ModelForm):
    first_name = forms.CharField(max_length=100, help_text='First Name')
    last_name = forms.CharField(max_length=100, help_text='Last Name')
    email = forms.EmailField(max_length=150, help_text='Email')

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.save()
        return instance

    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name']


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = [
            'phone', 'occupation', 'date_of_birth',
            'company_name', 'address', 'about',
            'company_telephone', 'picture', 'country'
        ]
