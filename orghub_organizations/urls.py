from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from organizations import views

from orghub_organizations.views import OrgHubOrganizationCreate, OrganizationList, OrgHubOrganizationDetail, \
    OrgHubOrganizationUpdate, OrgHubOrganizationPeopleAdd

urlpatterns = [
    # Organization URLs
    url(r'^$',
        view=OrganizationList.as_view(),
        name="organization_list"),
    url(r'^add/$',
        view=OrgHubOrganizationCreate.as_view(),
        name="organization_add"),
    url(r'^(?P<organization_pk>[\d]+)/$',
        view=OrgHubOrganizationDetail.as_view(),
        name="organization_detail"),
    url(r'^(?P<organization_pk>[\d]+)/edit/$',
        view=OrgHubOrganizationUpdate.as_view(),
        name="organization_edit"),
    url(r'^(?P<organization_pk>[\d]+)/delete/$',
        view=login_required(views.OrganizationDelete.as_view()),
        name="organization_delete"),

    # Organization user URLs
    url(r'^(?P<organization_pk>[\d]+)/people/$',
        view=login_required(views.OrganizationUserList.as_view()),
        name="organization_user_list"),
    # url(r'^(?P<organization_pk>[\d]+)/people/add/$',
    #     view=login_required(OrgHubOrganizationPeopleAdd.as_view()),
    #     name="organization_user_add"),
    # url(r'^(?P<organization_pk>[\d]+)/people/(?P<user_pk>[\d]+)/remind/$',
    #     view=login_required(views.OrganizationUserRemind.as_view()),
    #     name="organization_user_remind"),
    # url(r'^(?P<organization_pk>[\d]+)/people/(?P<user_pk>[\d]+)/$',
    #     view=login_required(views.OrganizationUserDetail.as_view()),
    #     name="organization_user_detail"),
    # url(r'^(?P<organization_pk>[\d]+)/people/(?P<user_pk>[\d]+)/edit/$',
    #     view=login_required(views.OrganizationUserUpdate.as_view()),
    #     name="organization_user_edit"),
    # url(r'^(?P<organization_pk>[\d]+)/people/(?P<user_pk>[\d]+)/delete/$',
    #     view=login_required(views.OrganizationUserDelete.as_view()),
    #     name="organization_user_delete"),
]
