from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.urls import reverse
from django.views.generic import ListView
from organizations.forms import OrganizationUserAddForm
from organizations.models import Organization, OrganizationUser
from organizations.views import BaseOrganizationCreate, OrganizationDetail, BaseOrganizationUpdate, \
    OrganizationUserCreate

from orghub_organizations.forms import OrgHubOrganizationAddForm, OrgHubOrganizationForm


class OrgHubOrganizationCreate(LoginRequiredMixin, BaseOrganizationCreate):
    model = Organization
    form_class = OrgHubOrganizationAddForm
    template_name = 'orghub_organizations/organization_edit.html'
    login_url = '/login'

    def get_success_url(self):
        return reverse("organization_list")

    def get_form_kwargs(self):
        kwargs = super(BaseOrganizationCreate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs


class OrgHubOrganizationUpdate(LoginRequiredMixin, BaseOrganizationUpdate):
    model = Organization
    form_class = OrgHubOrganizationForm
    template_name = 'orghub_organizations/organization_edit.html'
    login_url = '/login'

    def get_success_url(self):
        return reverse("organization_list")

    def get_form_kwargs(self):
        kwargs = super(OrgHubOrganizationUpdate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs


class OrganizationList(LoginRequiredMixin, ListView):
    template_name = 'orghub_organizations/organization_list.html'
    model = Organization
    context_object_name = 'object_list'
    paginate_by = 10
    login_url = '/login'

    def get_queryset(self):
        search_term = self.request.GET.get('search_term', '').strip()
        request_user = self.request.user

        queryset = self.model.objects \
            .filter(users=request_user)
        if search_term:
            queryset = queryset.filter(
                Q(name__icontains=search_term)
            )
        return queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        """Get the context for this view."""
        context = super().get_context_data(**kwargs)
        search_term = self.request.GET.get('search_term', '')
        context['search_term'] = search_term
        return context


class OrgHubOrganizationDetail(LoginRequiredMixin, OrganizationDetail):
    template_name = 'orghub_organizations/organization_detail.html'
    model = Organization
    paginate_by = 10
    login_url = '/login'

    def get_queryset(self):
        search_term = self.request.GET.get('search_term', '').strip()
        request_user = self.request.user

        queryset = self.model.objects \
            .filter(users=request_user)
        if search_term:
            queryset = queryset.filter(
                Q(name__icontains=search_term)
            )
        return queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        """Get the context for this view."""
        context = super().get_context_data(**kwargs)
        search_term = self.request.GET.get('search_term', '')
        context['search_term'] = search_term
        return context


class OrgHubOrganizationPeopleAdd(LoginRequiredMixin, OrganizationUserCreate):
    model = OrganizationUser
    form_class = OrganizationUserAddForm
    template_name = 'orghub_organizations/organization_people_add.html'
    login_url = '/login'

    def get_success_url(self):
        return reverse("organization_list")

    def get_form_kwargs(self):
        kwargs = super(OrgHubOrganizationPeopleAdd, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs
