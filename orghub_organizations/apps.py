from django.apps import AppConfig


class OrghubOrganizationsConfig(AppConfig):
    name = 'orghub_organizations'
