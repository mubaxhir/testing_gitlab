import random

from django import forms
from django.contrib.auth import get_user_model
from django.contrib.sites.shortcuts import get_current_site
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from organizations.backends import invitation_backend
from organizations.forms import OrganizationAddForm
from organizations.models import Organization, OrganizationUser
from organizations.utils import create_organization


class OrgHubOrganizationAddForm(OrganizationAddForm):
    """
    Form class for creating a new organization, complete with new owner, including a
    User instance, OrganizationUser instance, and OrganizationOwner instance.
    """

    class Meta(OrganizationAddForm.Meta):
        model = Organization
        exclude = ('slug', 'is_active', 'users',)

    def save(self, **kwargs):
        """
        Create the organization, then get the user, then make the owner.
        """
        is_active = True
        try:
            user = get_user_model().objects.get(email=self.cleaned_data['email'])
        except get_user_model().DoesNotExist:
            user = invitation_backend().invite_by_email(
                self.cleaned_data['email'],
                **{'domain': get_current_site(self.request),
                   'organization': self.cleaned_data['name'],
                   'sender': self.request.user, 'created': True})
            is_active = False
        slug = slugify(self.cleaned_data['name']) + '-' + str(random.randint(1, 20)).zfill(2)
        return create_organization(user, self.cleaned_data['name'],
                                   slug, is_active=is_active)


class OrgHubOrganizationForm(forms.ModelForm):
    """Form class for updating Organizations"""
    owner = forms.ModelChoiceField(OrganizationUser.objects.all())

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(OrgHubOrganizationForm, self).__init__(*args, **kwargs)
        self.fields['owner'].queryset = self.instance.organization_users.filter(
            is_admin=True, user__is_active=True)
        self.fields['owner'].initial = self.instance.owner.organization_user

    class Meta:
        model = Organization
        exclude = ('users', 'is_active')

    def save(self, commit=True):
        if self.instance.owner.organization_user != self.cleaned_data['owner']:
            self.instance.change_owner(self.cleaned_data['owner'])
        return super(OrgHubOrganizationForm, self).save(commit=commit)

    def clean_owner(self):
        owner = self.cleaned_data['owner']
        if owner != self.instance.owner.organization_user:
            if self.request.user != self.instance.owner.organization_user.user:
                raise forms.ValidationError(_("Only the organization owner can change ownerhip"))
        return owner
