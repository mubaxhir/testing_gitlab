from django.urls import path

# app_name = "organizations"
from accounts.views import PasswordReset, PasswordResetConfirm, PasswordResetComplete, PasswordResetDone, \
    PasswordChange, PasswordChangeDone

urlpatterns = [
    path('password_change/', PasswordChange.as_view(), name='password_change'),
    path('password_change/done/', PasswordChangeDone.as_view(), name='password_change_done'),
    path('password_reset/', PasswordReset.as_view(), name='password_reset'),
    path('password_reset/done/', PasswordResetDone.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', PasswordResetConfirm.as_view(), name='password_reset_confirm'),
    path('reset/done/', PasswordResetComplete.as_view(), name='password_reset_complete'),
]
