from django import template

register = template.Library()


@register.filter(name='plus_two_years')
def plus_two_years(value):
    return value.replace(year=value.year + 2)
