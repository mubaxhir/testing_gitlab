from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import PasswordResetView, PasswordResetConfirmView, PasswordResetCompleteView, \
    PasswordResetDoneView, PasswordChangeView, PasswordChangeDoneView
from django.db.models import Q
from django.views.generic import ListView
from organizations.models import OrganizationUser


class UserDirectoryView(LoginRequiredMixin, ListView):
    template_name = 'accounts/user_directory.html'
    model = OrganizationUser
    context_object_name = 'object_list'
    paginate_by = 10

    def get_context_data(self, *, object_list=None, **kwargs):
        """Get the context for this view."""
        context = super().get_context_data(**kwargs)
        search_term = self.request.GET.get('search_term', '')
        context['search_term'] = search_term
        if self.request.user.organizations_organizationuser.first().is_admin:
            organizations = OrganizationUser.objects.values('organization__name', 'pk')
        else:
            organizations = OrganizationUser.objects \
                .filter(user_id=self.request.user.pk) \
                .values('organization__name', 'pk')
        context['organizations'] = organizations
        return context

    def get_queryset(self):
        search_term = self.request.GET.get('search_term', '').strip()
        organization = self.request.GET.get('organization', '').strip()
        request_user = self.request.user
        if organization:
            org_ids = OrganizationUser.objects.filter(organization_id=organization).values_list('pk', flat=True)
        else:
            org_ids = request_user.organizations_organizationuser.values_list('pk', flat=True)
        queryset = self.model.objects \
            .filter(organization__id__in=org_ids) \
            .order_by('user__username') \
            .distinct('user__username')
        if search_term:
            queryset = queryset.filter(
                Q(organization__name__icontains=search_term) |
                Q(user__first_name__icontains=search_term) |
                Q(user__last_name__icontains=search_term) |
                Q(user__username__icontains=search_term) |
                Q(user__email__icontains=search_term)
            )
        return queryset


class PasswordReset(PasswordResetView):
    template_name = 'accounts/password_reset_form.html'


class PasswordResetDone(PasswordResetDoneView):
    template_name = 'accounts/password_reset_done.html'


class PasswordResetConfirm(PasswordResetConfirmView):
    template_name = 'accounts/password_reset_confirm.html'


class PasswordResetComplete(PasswordResetCompleteView):
    template_name = 'accounts/password_reset_complete.html'


class PasswordChange(PasswordChangeView):
    template_name = 'accounts/password_change_form.html'


class PasswordChangeDone(PasswordChangeDoneView):
    template_name = 'accounts/password_change_done.html'
